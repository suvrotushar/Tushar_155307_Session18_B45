<?php
require_once("../../../vendor/autoload.php");

use \OOP\Penguin;
use \OOP\Dove;
use \OOP\canFly;
use \OOP\canSwim;
use \OOP\Duck;
$obj = new \OOP\Test();

//////////////////////////////////

$obj->sayHello();
$obj->sayWorld();
$obj->sayExaclamationMark();

////////////////////////////////

$objAbstract = new \OOP\InheritedFromAbstractClass();
$objAbstract->setMyValue("Hello Abstract class! <br>");

echo $objAbstract->getMyValue();
//////////////////////// Bird Using Imple/////////////




describeBird(new Penguin());


function describeBird($bird){
    if($bird instanceof Bird){
        $bird->info();
    }
    if ($bird instanceof canFly){
        $bird->fly();
    }
    if ($bird instanceof canSwim){
        $bird->swim();
    }
}

describeBird(new Duck());

describeBird(new Dove());
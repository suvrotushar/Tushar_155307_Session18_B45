<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Collection Form</title>
</head>
<body>

<form action="process.php" method="post">
    Enter Student's Name:
    <input type="text" name="name">
    <br>

    Enter Student's Roll:
    <input type="text" name="roll">
    <br>

    Bangla Mark:
    <input type="number" step="any" min="0" max="100" name="markBangla">
    <br>

    English Mark:
    <input type="number" step="any" min="0" max="100" name="markEnglish">
    <br>

    Math Mark:
    <input type="number" step="any" min="0" max="100" name="markMath">
    <br>

    <input type="submit">

</form>
</body>
</html>
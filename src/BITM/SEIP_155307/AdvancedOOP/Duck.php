<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 3:34 PM
 */

namespace OOP;

class Duck extends Bird implements canFly, canSwim
{
    public $name = "Duck";

    public function fly()
    {
        echo "I am $this->name ...I can Fly<br>";
    }

    public function swim()
    {
        echo "I am $this->name ...I can swim<br>";
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 3:33 PM
 */

namespace OOP;


class Penguin extends Bird implements canSwim{
    public  $name = "Penguin";
    public function swim()
    {
        echo "I am Penguin...I can Swim<br>";
    }
}